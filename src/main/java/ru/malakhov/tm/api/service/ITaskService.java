package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable Task task);

    void remove(@Nullable String userId, @Nullable Task task);

    @NotNull
    List<Task> findAll(@Nullable String userId);

    void clear(@Nullable String userId);

    Task findOneById(@Nullable String userId, @Nullable  String id);

    Task findOneByIndex(@Nullable String userId, @Nullable  Integer index);

    Task findOneByName(@Nullable String userId, @Nullable  String name);

    Task removeOneById(@Nullable String userId, @Nullable  String id);

    Task removeOneByIndex(@Nullable String userId, @Nullable  Integer index);

    Task removeOneByName(@Nullable String userId, @Nullable String name);

    Task updateTaskById(@Nullable String userId, @Nullable  String id, @Nullable  String name,
                        @Nullable String description);

    Task updateTaskByIndex(@Nullable String userId, @Nullable  Integer index, @Nullable String name,
                           @Nullable String description);

    Task merge(@Nullable Task task);

    void merge(@Nullable Task... tasks);

    void merge(@Nullable Collection<Task> tasks);

    void load(@Nullable Task... tasks);

    void load(@Nullable Collection<Task> tasks);

    void clear();

    @NotNull
    List<Task> getTaskList();


}