package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.model.Role;

public interface IAuthService {

    @NotNull
    String getUserId();

    void login(@Nullable String login, @Nullable String password);

    void logout();

    void checkRole(@Nullable Role[] roles);

}