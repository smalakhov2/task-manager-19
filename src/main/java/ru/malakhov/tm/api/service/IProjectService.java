package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    void create(@Nullable String userId, @Nullable  String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable Project project);

    void remove(@Nullable String userId, @Nullable Project project);

    @NotNull
    List<Project> findAll(@Nullable String userId);

    void clear(@Nullable String userId);

    Project findOneById(@Nullable String userId, @Nullable String id);

    Project findOneByIndex(@Nullable String userId, @Nullable  Integer index);

    Project findOneByName(@Nullable String userId, @Nullable String name);

    Project removeOneById(@Nullable String userId, @Nullable String id);

    Project removeOneByIndex(@Nullable String userId, @Nullable  Integer index);

    Project removeOneByName(@Nullable String userId, @Nullable  String name);

    Project updateProjectById(@Nullable String userId, @Nullable  String id, @Nullable  String name,
                              @Nullable String description);

    Project updateProjectByIndex(@Nullable String userId, @Nullable Integer index,
                                 @Nullable  String name, @Nullable String description);

    Project merge(@Nullable Project project);

    void merge(@Nullable Project... projects);

    void merge(@Nullable Collection<Project> projects);

    void load(@Nullable Project... projects);

    void load(@Nullable Collection<Project> projects);

    void clear();

    @NotNull
    List<Project> getProjectList();

}