package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.model.Role;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    List<User> findAll();

    User create(@Nullable String login, @Nullable String password);

    User create(@Nullable String login, @Nullable String password,@Nullable String email);

    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    User findById(@Nullable String id);

    User findByLogin(@Nullable String login);

    User removeUser(@Nullable User user);

    User removeById(@Nullable String id);

    User removeByLogin(@Nullable String login);

    @NotNull
    String[] profile(@Nullable String id);

    User changeEmail(@Nullable String id, @Nullable String email);

    User changePassword(@Nullable String id, @Nullable String password);

    User changeLogin(@Nullable String id, @Nullable String login);

    User changeFirstName(@Nullable String id, @Nullable String name);

    User changeMiddleName(@Nullable String id, @Nullable String name);

    User changeLastName(@Nullable String id, @Nullable String name);

    User lockUserByLogin(@Nullable String login);

    User unlockUserByLogin(@Nullable String login);

    void clear();

    User merge(@Nullable User user);

    void merge(@Nullable User... users);

    void merge(@Nullable Collection<User> users);

    void load(@Nullable User... users);

    void load(@Nullable Collection<User> users);

    @NotNull
    List<User> getUserList();

}