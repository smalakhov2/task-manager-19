package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository {

    void add(@NotNull String userId, @NotNull Project project);

    void remove(@NotNull String userId, @NotNull Project project);

    @NotNull
    List<Project> findAll(@NotNull String userId);

    void clear(@NotNull String userId);

    Project findOneById(@NotNull String userId, @NotNull String id);

    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    Project findOneByName(@NotNull String userId, @NotNull String name);

    Project removeOneById(@NotNull String userId, @NotNull String id);

    Project removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    Project removeOneByName(@NotNull String userId, @NotNull String name);

    Project merge(@NotNull Project project);

    void merge(@NotNull Project... projects);

    void merge(@NotNull Collection<Project> projects);

    void load(@NotNull Project... projects);

    void load(@NotNull Collection<Project> projects);

    void clear();

    @NotNull
    List<Project> getProjectList();

}