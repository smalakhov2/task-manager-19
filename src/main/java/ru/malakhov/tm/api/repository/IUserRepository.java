package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.entity.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository {

    @NotNull
    List<User> findAll();

    User add(@NotNull User user);

    User findById(@NotNull String id);

    User findByLogin(@NotNull String login);

    User removeUser(@NotNull User user);

    User removeById(@NotNull String id);

    User removeByLogin(@NotNull String login);

    @NotNull
    String[] profile(@NotNull String id);

    User changeEmail(@NotNull String id, @NotNull String email);

    User changePassword(@NotNull String id, @NotNull String password);

    User changeLogin(@NotNull String id, @NotNull String login);

    User changeFirstName(@NotNull String id, @NotNull String name);

    User changeMiddleName(@NotNull String id, @NotNull String name);

    User changeLastName(@NotNull String id, @NotNull String name);

    void clear();

    User merge(@NotNull User user);

    void merge(@NotNull User... users);

    void merge(@NotNull Collection<User> users);

    void load(@NotNull User... users);

    void load(@NotNull Collection<User> users);

    @NotNull
    List<User> getUserList();

}