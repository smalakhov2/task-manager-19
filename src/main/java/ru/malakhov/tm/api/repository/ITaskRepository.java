package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository {

    void add(@NotNull String userId, @NotNull Task task);

    void remove(@NotNull String userId, @NotNull Task task);

    @NotNull
    List<Task> findAll(@NotNull String userId);

    void clear(@NotNull String userId);

    Task findOneById(@NotNull String userId, @NotNull String id);

    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    Task findOneByName(@NotNull String userId, @NotNull String name);

    Task removeOneById(@NotNull String userId, @NotNull String id);

    Task removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    Task removeOneByName(@NotNull String userId, @NotNull String name);

    Task merge(@NotNull Task task);

    void merge(@NotNull Task... tasks);

    void merge(@NotNull Collection<Task> tasks);

    void load(@NotNull Task... tasks);

    void load(@NotNull Collection<Task> tasks);

    void clear();

    @NotNull
    List<Task> getTaskList();

}