package ru.malakhov.tm.command.data.fasterxml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.service.IDomainService;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;
import ru.malakhov.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataFasterxmlLoadXmlCommand extends AbstractCommand {
    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-fasterxml-xml-load";
    }

    @Override
    public @NotNull String description() {
        return "Load xml fasterxml date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA FASTERXML XML LOAD]");
        final @NotNull String xml = new String(Files.readAllBytes(Paths.get(DataConst.DATA_FASTERXML_XML_PATH)));

        final @NotNull XmlMapper xmlMapper = new XmlMapper();
        Domain domain = xmlMapper.readValue(xml, Domain.class);

        final @NotNull IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);

        System.out.println("[OK]");
    }

}