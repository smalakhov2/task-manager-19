package ru.malakhov.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.service.IDomainService;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.dto.Domain;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

import static ru.malakhov.tm.constant.DataConst.BINARY_PATH;

public class DataBinaryLoadCommand extends AbstractCommand {
    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "binary-load";
    }

    @Override
    public @NotNull String description() {
        return "Load binary data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY LOAD]");
        final @NotNull FileInputStream fileInputStream = new FileInputStream(BINARY_PATH);
        final @NotNull ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        final @NotNull Domain domain = (Domain) objectInputStream.readObject();
        final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        objectInputStream.close();
        fileInputStream.close();
        System.out.println("[OK]");

    }

}