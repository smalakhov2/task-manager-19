package ru.malakhov.tm.command.data.fasterxml;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.service.IDomainService;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.DataConst;
import ru.malakhov.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataFasterxmlLoadJsonCommand extends AbstractCommand {
    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-fasterxml-json-load";
    }

    @Override
    public @NotNull String description() {
        return "Load json fasterxml date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA FASTERXML JSON LOAD]");
        final @NotNull String json = new String(Files.readAllBytes(Paths.get(DataConst.DATA_FASTERXML_JSON_PATH)));

        final @NotNull ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);
        final @NotNull Domain domain = objectMapper.readValue(json, Domain.class);

        final @NotNull IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);

        System.out.println("[OK]");
    }

}