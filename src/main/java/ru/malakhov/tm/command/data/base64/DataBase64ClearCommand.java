package ru.malakhov.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;

import java.io.File;
import java.nio.file.Files;

import static ru.malakhov.tm.constant.DataConst.BASE64_PATH;

public class DataBase64ClearCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "db64-clear";
    }

    @Override
    public @NotNull String description() {
        return "Clear base64 data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 CLEAR]");
        final @NotNull File file = new File(BASE64_PATH);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");

    }

}