package ru.malakhov.tm.command.data.base64;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.dto.Domain;
import sun.misc.BASE64Encoder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

import static ru.malakhov.tm.constant.DataConst.BASE64_PATH;

public class DataBase64SaveCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "db64-save";
    }

    @Override
    public @NotNull String description() {
        return "Save base64 data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 SAVE]");
        final @NotNull Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);

        final @NotNull File file = new File(BASE64_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final @NotNull ByteOutputStream byteOutputStream = new ByteOutputStream();
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteOutputStream.close();

        final byte[] bytes = byteOutputStream.toByteArray();
        final String base64 = new BASE64Encoder().encode(bytes);

        final FileOutputStream fileOutputStream = new FileOutputStream(BASE64_PATH);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
        System.out.println("[OK]");
    }

}