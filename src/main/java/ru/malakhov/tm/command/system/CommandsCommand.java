package ru.malakhov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.ArgumentConst;
import ru.malakhov.tm.constant.TerminalConst;

import java.util.List;

public final class CommandsCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return ArgumentConst.COMMANDS;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.COMMANDS;
    }

    @Override
    public @NotNull String description() {
        return "Display commands.";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final @NotNull List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (final @NotNull AbstractCommand command: commands) {
            System.out.println(command.name() + " - " + command.description());
        }
        System.out.println("[OK]");
    }

}