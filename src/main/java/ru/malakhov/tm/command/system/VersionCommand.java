package ru.malakhov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.ArgumentConst;
import ru.malakhov.tm.constant.TerminalConst;

public final class VersionCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return ArgumentConst.VERSION;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.VERSION;
    }

    @Override
    public @NotNull String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.1.9");
    }

}
