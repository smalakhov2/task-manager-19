package ru.malakhov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;

public final class ExitCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.EXIT;
    }

    @Override
    public @NotNull String description() {
        return "Close application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}