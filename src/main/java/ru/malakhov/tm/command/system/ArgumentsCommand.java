package ru.malakhov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.ArgumentConst;
import ru.malakhov.tm.constant.TerminalConst;

import java.util.List;

public final class ArgumentsCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return ArgumentConst.ARGUMENTS;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.ARGUMENTS;
    }

    @Override
    public @NotNull String description() {
        return "Display arguments.";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        final @NotNull List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (final @NotNull AbstractCommand command: commands) {
            if (command.argument() == null) continue;
            System.out.println(command.argument() + " - " + command.description());
        }
        System.out.println("[OK]");
    }

}