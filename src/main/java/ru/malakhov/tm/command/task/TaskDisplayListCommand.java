package ru.malakhov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.entity.Task;

import java.util.List;

public final class TaskDisplayListCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.TASK_LIST;
    }

    @Override
    public @NotNull  String description() {
        return "Display task list.";
    }

    @Override
    public void execute() {
        final @Nullable String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LIST TASKS]");
        final @NotNull List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        for (final @NotNull Task task : tasks) System.out.println(task);
        System.out.println("[OK]");
    }

}