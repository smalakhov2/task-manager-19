package ru.malakhov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.entity.Project;

import java.util.List;

public final class ProjectDisplayListCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.PROJECT_LIST;
    }

    @Override
    public @NotNull String description() {
        return "Display task projects.";
    }

    @Override
    public void execute() {
        final @Nullable String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[DISPlAY PROJECTS]");
        final @NotNull List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        for (final @NotNull Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

}