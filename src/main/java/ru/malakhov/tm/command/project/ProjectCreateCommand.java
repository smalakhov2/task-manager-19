package ru.malakhov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.PROJECT_CREATE;
    }

    @Override
    public @NotNull String description() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        final @Nullable String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final @Nullable String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final @Nullable String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().create(userId, name, description);
        System.out.println("[OK]");
    }

}