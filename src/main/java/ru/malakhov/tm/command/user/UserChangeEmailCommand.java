package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserChangeEmailCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.CHANGE_EMAIL;
    }

    @Override
    public @NotNull String description() {
        return "Change e-mail.";
    }

    @Override
    public void execute() {
        final @Nullable String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE E-MAIL]");
        System.out.println("ENTER NEW E-MAIL:");
        final @Nullable String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().changeEmail(userId, email);
        System.out.println("[OK]");
    }

}