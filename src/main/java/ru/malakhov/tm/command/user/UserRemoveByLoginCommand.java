package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.model.Role;
import ru.malakhov.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.REMOVE_USER_BY_LOGIN;
    }

    @Override
    public @NotNull String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        final @Nullable String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public @Nullable Role[] role() {
        return new Role[] {Role.ADMIN};
    }

}