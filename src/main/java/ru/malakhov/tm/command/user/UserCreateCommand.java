package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserCreateCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.REGISTRY;
    }

    @Override
    public @NotNull String description() {
        return "Registry";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("Enter login:");
        final @Nullable  String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        final @Nullable String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().create(login, password);
        System.out.println("[OK]");
    }

}
