package ru.malakhov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.model.Role;
import ru.malakhov.tm.util.TerminalUtil;

public class UserRemoveByIdCommand extends AbstractCommand {

    @Override
    public @Nullable String argument() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return TerminalConst.REMOVE_USER_BY_ID;
    }

    @Override
    public @NotNull String description() {
        return "Remove user by id.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER ID:");
        final @Nullable String id = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeById(id);
        System.out.println("[OK]");
    }

    @Override
    public @Nullable Role[] role() {
        return new Role[] {Role.ADMIN};
    }

}