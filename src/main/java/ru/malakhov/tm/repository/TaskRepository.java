package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.exception.task.TaskNotFoundException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final @NotNull List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final @NotNull String userId, final @NotNull Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void remove(final @NotNull String userId, final @NotNull Task task) {
        if (!userId.equals(task.getUserId())) return;
        tasks.remove(task);
    }

    @Override
    public @NotNull List<Task> findAll(final @NotNull String userId) {
        final List<Task> result = new ArrayList<>();
        for (final @NotNull Task task: tasks){
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final @NotNull String userId) {
        final List<Task> tasks = findAll(userId);
        this.tasks.removeAll(tasks);
    }

    @Override
    public @NotNull Task findOneById(final @NotNull String userId, final @NotNull String id) {
        for (final @NotNull Task task : tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (id.equals(task.getId())) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public @NotNull Task findOneByIndex(final @NotNull String userId, final @NotNull Integer index) {
        if (!userId.equals(tasks.get(index).getUserId())) throw new TaskNotFoundException();
        return tasks.get(index);
    }

    @Override
    public @NotNull Task findOneByName(final @NotNull String userId, final @NotNull String name) {
        for (final @NotNull Task task : tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public Task removeOneById(final @NotNull String userId, final @NotNull String id) {
        final @NotNull Task task = findOneById(userId, id);
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeOneByIndex(final @NotNull String userId, final  @NotNull Integer index) {
        final @NotNull Task task = findOneByIndex(userId, index);
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeOneByName(final @NotNull String userId, final @NotNull String name) {
        final @NotNull Task task = findOneByName(userId, name);
        remove(userId, task);
        return task;
    }

    @Override
    public Task merge(final @NotNull Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void merge(final @NotNull Task... tasks) {
        for (final @NotNull Task task: tasks) merge(task);
    }

    @Override
    public void merge(final @NotNull Collection<Task> tasks) {
        for (final Task task: tasks) merge(task);
    }

    @Override
    public void load(final @NotNull Task... tasks) {
        clear();
        merge(tasks);
    }

    @Override
    public void load(final @NotNull Collection<Task> tasks) {
        clear();
        merge(tasks);
    }

    @Override
    public void clear() {
        tasks.removeAll(tasks);
    }

    @Override
    public @NotNull List<Task> getTaskList() {
        return new @NotNull ArrayList<>(tasks);
    }

}