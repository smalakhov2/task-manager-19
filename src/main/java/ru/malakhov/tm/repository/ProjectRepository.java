package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.exception.project.ProjectNotFoundException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final @NotNull List<Project> projects = new ArrayList<>();

    @Override
    public void add(final @NotNull String userId, final @NotNull Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public void remove(final @NotNull String userId, final @NotNull Project project) {
        if (!userId.equals(project.getUserId())) return;
        projects.remove(project);
    }

    @Override
    public @NotNull List<Project> findAll(final @NotNull String userId) {
        final @NotNull List<Project> result = new ArrayList<>();
        for (final @NotNull Project project: projects){
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(final @NotNull String userId) {
        final @NotNull List<Project> projects = findAll(userId);
        this.projects.removeAll(projects);
    }

    @Override
    public @NotNull Project findOneById(final @NotNull String userId, final @NotNull String id) {
        for (final @NotNull Project project : projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (id.equals(project.getId())) return project;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public @NotNull Project findOneByIndex(final @NotNull String userId, final @NotNull Integer index) {
        if (!userId.equals(projects.get(index).getUserId())) throw new ProjectNotFoundException();
        return projects.get(index);
    }

    @Override
    public @NotNull Project findOneByName(final @NotNull String userId, final @NotNull String name) {
        for (final @NotNull Project project : projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (name.equals(project.getName())) return project;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public Project removeOneById(final @NotNull String userId, final @NotNull String id) {
        final @NotNull Project project = findOneById(userId, id);
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeOneByIndex(final @NotNull String userId, final @NotNull Integer index) {
        final @NotNull Project project = findOneByIndex(userId, index);
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeOneByName(final @NotNull String userId, final @NotNull String name) {
        final @NotNull Project project = findOneByName(userId, name);
        remove(userId, project);
        return project;
    }

    @Override
    public Project merge(final @NotNull Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void merge(final @NotNull Project... projects) {
        if (projects == null) return;
        for (final Project project: projects) merge(project);
    }

    @Override
    public void merge(final @NotNull Collection<Project> projects) {
        for (final @NotNull Project project: projects) merge(project);
    }

    @Override
    public void load(final @NotNull Project... projects) {
        clear();
        merge(projects);
    }

    @Override
    public void load(final @NotNull Collection<Project> projects) {
        clear();
        merge(projects);
    }

    @Override
    public void clear() {
        projects.removeAll(projects);
    }

    @Override
    public @NotNull List<Project> getProjectList() {
        return new @NotNull ArrayList<>(projects);
    }

}