package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.exception.user.UserNotFoundException;
import ru.malakhov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class UserRepository implements IUserRepository {

    private final @NotNull List<User> users = new ArrayList<>();

    @Override
    public @NotNull List<User> findAll() {
        return null;
    }

    @Override
    public @NotNull User add(final @NotNull User user) {
        users.add(user);
        return user;
    }

    @Override
    public @NotNull User findById(final @NotNull String id) {
        for (final @NotNull User user : users) {
            if (id.equals(user.getId())) return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public @NotNull User findByLogin(final @NotNull String login) {
        for (final @NotNull User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public @NotNull User removeUser(final @NotNull User user) {
        users.remove(user);
        return user;
    }

    @Override
    public @NotNull User removeById(final @NotNull String id) {
        final User user = findById(id);
        return removeUser(user);
    }

    @Override
    public @NotNull User removeByLogin(final @NotNull String login) {
        final User user = findByLogin(login);
        return removeUser(user);
    }

    @Override
    public @NotNull String[] profile(final @NotNull String id) {
        @NotNull String[] profileInfo = {"First name: " + findById(id).getFirstName(),
                                "Last name: " + findById(id).getLastName(),
                                "Middle name: " + findById(id).getMiddleName(),
                                "Login: " + findById(id).getLogin(),
                                "E-mail: " + findById(id).getEmail(),
                                "Account type: " + findById(id).getRole().getDisplayName()};
        return profileInfo;
    }

    @Override
    public @NotNull User changeEmail(final @NotNull String id, final @NotNull String email) {
        final User user = findById(id);
        user.setEmail(email);
        return user;
    }

    @Override
    public @NotNull User changePassword(final @NotNull String id, final @NotNull String password) {
        final @NotNull User user = findById(id);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public @NotNull User changeLogin(final @NotNull String id, final @NotNull String login) {
        final User user = findById(id);
        user.setLogin(login);
        return user;
    }

    @Override
    public @NotNull User changeFirstName(final @NotNull String id, final @NotNull String name) {
        final User user = findById(id);
        user.setFirstName(name);
        return user;
    }

    @Override
    public @NotNull User changeMiddleName(final @NotNull String id, final @NotNull String name) {
        final User user = findById(id);
        user.setMiddleName(name);
        return user;
    }

    @Override
    public @NotNull User changeLastName(final @NotNull String id, final @NotNull String name) {
        final User user = findById(id);
        user.setLastName(name);
        return user;
    }

    @Override
    public void clear() {
        users.removeAll(users);
    }

    @Override
    public @NotNull User merge(final @NotNull User user) {
        users.add(user);
        return user;
    }

    @Override
    public void merge(final @NotNull User... users) {
        for (final @NotNull User user: users) merge(user);
    }

    @Override
    public void merge(final @NotNull Collection<User> users) {
        for (final @NotNull User user: users) merge(user);
    }

    @Override
    public void load(final @NotNull User... users) {
        clear();
        merge(users);
    }

    @Override
    public void load(final @NotNull Collection<User> users) {
        clear();
        merge(users);
    }

    @Override
    public @NotNull List<User> getUserList() {
        return new @NotNull ArrayList<>(users);
    }

}