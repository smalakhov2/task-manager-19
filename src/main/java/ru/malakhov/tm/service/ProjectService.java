package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.exception.empty.EmptyDescriptionException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.system.IncorrectIndexException;
import ru.malakhov.tm.exception.empty.EmptyNameException;

import java.util.Collection;
import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(final @Nullable String userId, final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    public void add(final @Nullable String userId, final @Nullable Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.add(userId, project);
    }

    @Override
    public void remove(final @Nullable String userId, final @Nullable Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.remove(userId, project);
    }

    @Override
    public List<Project> findAll(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findAll(userId);
    }

    @Override
    public void clear(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.clear(userId);
    }

    @Override
    public Project findOneById(final @Nullable String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneById(userId, id);
    }

    @Override
    public Project findOneByIndex(final @Nullable String userId, final @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Override
    public Project findOneByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }

    @Override
    public Project removeOneById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeOneById(userId, id);
    }

    @Override
    public Project removeOneByIndex(final @Nullable String userId, final @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Project removeOneByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeOneByName(userId, name);
    }

    @Override
    public Project updateProjectById(final @Nullable String userId, @Nullable final String id,
                                     final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(userId, id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final @Nullable String userId, final @Nullable Integer index,
                                        final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project merge(final @Nullable Project project) {
        if (project == null) return null;
        return projectRepository.merge(project);
    }

    @Override
    public void merge(final @Nullable Project... projects) {
        if (projects == null) return;
        projectRepository.merge(projects);
    }

    @Override
    public void merge(final @Nullable Collection<Project> projects) {
        if (projects == null) return;
        projectRepository.merge(projects);
    }

    @Override
    public void load(final @Nullable Project... projects) {
        if (projects == null) return;
        projectRepository.load(projects);
    }

    @Override
    public void load(final @Nullable Collection<Project> projects) {
        if (projects == null) return;
        projectRepository.load(projects);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public @NotNull List<Project> getProjectList() {
        return projectRepository.getProjectList();
    }

}