package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.system.IncorrectIndexException;
import ru.malakhov.tm.exception.empty.EmptyNameException;

import java.util.Collection;
import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(userId, task);
    }

    @Override
    public void create(final @Nullable  String userId, final @Nullable String name,
                       final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
    }

    @Override
    public void add(final @Nullable String userId, final @Nullable Task task) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) return;
        taskRepository.add(userId, task);
    }

    @Override
    public void remove(final @Nullable String userId, final @Nullable Task task) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) return;
        taskRepository.remove(userId, task);
    }

    @Override
    public List<Task> findAll(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.findAll(userId);
    }

    @Override
    public void clear(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.clear(userId);
    }

    @Override
    public Task findOneById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findOneById(userId, id);
    }

    @Override
    public Task findOneByIndex(final @Nullable String userId, final @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @Override
    public Task findOneByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    public Task removeOneById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeOneById(userId, id);
    }

    @Override
    public Task removeOneByIndex(final @Nullable String userId, final @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Task removeOneByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeOneByName(userId, name);
    }

    @Override
    public Task updateTaskById(final @Nullable String userId, final @Nullable String id,
                               final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final @Nullable String userId, final @Nullable Integer index,
                                  final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task merge(final @Nullable Task task) {
        if (task == null) return null;
        return taskRepository.merge(task);
    }

    @Override
    public void merge(final @Nullable Task... tasks) {
        if (tasks == null) return;
        taskRepository.merge(tasks);
    }

    @Override
    public void merge(final @Nullable Collection<Task> tasks) {
        if (tasks == null) return;
        taskRepository.merge(tasks);
    }

    @Override
    public void load(final @Nullable Task... tasks) {
        if (tasks == null) return;
        taskRepository.load(tasks);
    }

    @Override
    public void load(final @Nullable Collection<Task> tasks) {
        if (tasks == null) return;
        taskRepository.load(tasks);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public @NotNull List<Task> getTaskList() {
        return taskRepository.getTaskList();
    }

}