package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.api.service.IAuthService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.model.Role;
import ru.malakhov.tm.util.HashUtil;
import ru.malakhov.tm.exception.empty.EmptyLoginException;
import ru.malakhov.tm.exception.empty.EmptyPasswordException;
import ru.malakhov.tm.exception.user.AccessDeniedException;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService){
        this.userService = userService;
    }

    @Override
    public @NotNull String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public void login(final @Nullable String login, final @Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final @NotNull User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()==true) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void checkRole(@Nullable Role[] roles) {
        if (roles == null || roles.length == 0) return;
        final String userId = getUserId();
        final User user = userService.findById(userId);
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role element: roles) if (role.equals(element)) return;
        throw new AccessDeniedException();
    }

}